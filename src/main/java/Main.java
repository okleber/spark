import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.SparkConf;
import scala.Tuple2;

import java.util.Arrays;

public class Main {

    public static void main(String[] args){

        //Create a SparkContext to initialize
        SparkConf conf = new SparkConf().setMaster("yarn").setAppName("Word Count");

        // Create a Java version of the Spark Context
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Load the text into a Spark RDD, which is a distributed representation of each line of text
        JavaRDD<String> textFile = sc.textFile("s3a://roothdfs/shakespeare.txt");
        JavaPairRDD<String, Integer> counts = textFile
                .flatMap(s -> Arrays.asList(s.split("[ ,]")).iterator())
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) -> a + b);
        counts.foreach(p -> System.out.println(p));
        System.out.println("Total words: " + counts.count());
        counts.saveAsTextFile("s3a://roothdfs/shakespeareWordCount");
    }

}


/*
go to command line in ./spark dir and run "mvn package"
copy the jar file to /tmp
create thequeue in yarn
copy shakespearefile to hdfs
spark-submit --class "Main" --master yarn --deploy-mode cluster --queue thequeue --driver-memory 512m --executor-memory 1024m --executor-cores 1 /tmp/spark-1.0-SNAPSHOT.jar
[hdfs@master1 ~]$ hadoop fs -ls s3a://roothdfs/
Found 1 items
drwxrwxrwx   - hdfs hdfs          0 2018-02-27 03:29 s3a://roothdfs/root
*/